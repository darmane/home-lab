# Monitoring

## Stack

- Grafana: Open-source platform for monitoring and observability.
- Prometheus: Systems and service monitoring system.
- Node exporter: Prometheus exporter for hardware and OS metrics exposed by \*NIX kernels, written in Go with pluggable metric collectors.

## Setup

1. Set environment variables in `.env` file.

2. Start services with `docker compose up -d`.
3. Log in Grafana with user `admin`and password `admin`
4. Add Prometheus as default data source with the URL pointing to the Prometheus container. URL: `http://prometheus:9090`
5. **OPTIONAL** - Import the [Node Exporter Full (ID: 1860)](https://grafana.com/grafana/dashboards/1860-node-exporter-full) dashboard to visualize server metrics.
