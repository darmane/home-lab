# HOME LAB

In this repository you will find a collection of some of the services that are running on my server.

## Stack

- Linux server
- Docker or Podman

## Philosofy

The goal is keep the host with the minimum number of open ports in the host machine, which are 80 (HTTP), 443 (HTTPS), 53 (DNS) and 22 (SSH), and reach all the services with subdomains through a reverse proxy. You may need to open more ports depending of the services that you are gonna run, e.g., an email server.

In addition, we do not want to install anything more than what is necessary in the host machine. This is the reason why each service is isolated in a container so that we can up and down services without worrying about.

![Traefik scheme](__assets__/traefik-scheme.png)

## Internet

I personally keep my services only on my local network, but you can reach them through the internet if you own a domain and a static IP. In this case you don't need to run Pi-hole.

## HTTPS

HTTPS connection is already configured even in your local network. To connect to your services via HTTPS you need to own a domain and a static IP, then write your domain in the Traefik configuration.

## Services

- Traefik: Reverse proxy, we will reach all our services through it with subdomains instead of expose the port of the service. [Visit website.](https://traefik.io)
- Pi-hole: DNS server that allows to redirect the trafic from our subdomain to the host's ip. [Visit website.](https://pi-hole.net)
- Portainer: Web-based container management platform. [Visit website.](https://www.portainer.io)
- Odoo: Open source ERP. [Visit website.](https://www.odoo.com)
- Taiga: Open source project management tool. [Visit website.](https://www.taiga.io)
- Monitoring: Server and apps monitoring tools powered by Grafana and Prometheus.

## License

MIT license.

**Hey!** Read the [LICENSE.md](LICENSE.md) carefully before doing anything with this project.