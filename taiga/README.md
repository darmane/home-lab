# Taiga

## Setup

1. Set environment variables in `.env` file.

2. Start services with `docker compose up -d`.
3. Create admin with the command below.
   ```bash
   sudo docker compose -f compose.yaml --profile manage run --rm taiga-manage createsuperuser
   ```
4. Restart the services with `docker compose restart`.
